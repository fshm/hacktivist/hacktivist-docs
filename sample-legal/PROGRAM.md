# Sample Program

# Abstract

A society's knowledge cannot be owned, appropriated or controlled by any one entity as its creation is the result of cumulative experiences in interaction with the members of the society. 

Time and time again, asymmetry in knowledge is used to create hegemony within society. This hegemony needs to be questioned, eroded and replaced with an knowledge commons based society for the welfare of the masses.

# Ethical technology 
The need for an ethical technology arises when we understand that merely FOSS (FSF & OSI definition) isn't enough. Ethical technology refers to the processes surrounding technologies in the context of the social groups it affects / impacts.

- An ethical technology can be Software, Hardware, design of a 

- An ethical technology should not be relicensed 

# Community, Movement & Organization

- Movement is the largest entity where people of different interests, background, associations come together for a larger goal of social change and/or political action.

- Communities are the fundamental unit of the movement which are engaged in the active, everyday political action in tandem with the movement. Communities can be individual by themselves or can exist as a group of communities.

- Organization exists for the sole purpose of continuity of the movement. Organization's role in the movement is to provide resources, support structure, contacts, legal representation, direction etc.

# Propaganda

Propagation of a Movement's values, stands, ideas to the masses is as important as the Movement's work itself. 

Propagation

# Membership


# Closed Technologies

Use of closed technologies should be avoided as much as possible in a hacktivist's everyday life.

Movement should never use any closed technology for its day-to-day functioning other than its presence on platforms for propagation.
