# Sample Constitution

# Name

<Name> Computing Society

# Insignia

# Objectives

- To work for the promotion, enrichment, adoption of Knowledge Commons in all walks of life. Knowledge Commons include 

- To create an inclusive, participative society in the creation and use of its knowledge for the benefit of society as a whole.

- To host software services for public benefit
 
- To engage in research and development

- To conduct meetups, classes, events, sessions, workshops or any other kind of educational, vocational, training sessions in place or via Internet for free or for a fee.

- To ally with other entities, societies, trusts, businesses as needed to achieve the objectives.

- To organize 

- 

# Membership
Membership can be 

## General Body Membership
- Must have a volunteer membership for a year
- 

## Volunteer  Membership
- Open to all 
- 

## Organizational Partner
- 

# Pledge 
I hereby pledge that I will work to enrich, protect and propagate the Knowledge Commons.

# Membership records
- Secretary will be offi
- 

# Checkup of membership


# Resignation of membership


# Membership Fee
- Membership can be charged annually. Fees can be 

## Students

## Employees

## Organization

# Duties of members 
- Active involvement in the Knowledge Commons
- Active involvement in the movement
- Take up tasks for the organization
- Attend their respective organizational meetings
- Work with the movement to 

# Rights of members
- To participate in decisions regarding the direction of the movement in short term or long term
- To be informed of everyday decisions
- To inspect the books of accounts
- To elect representatives to the Executive Committee

# Executive Committee
- Exists at the pleasure of the General Body
- Oversees day-to-day operations as decided annually by the General Body

## Formation
- Election happens at annual conference / General Body meeting
- Existing Executive Committee can nominate the next Executive Committee which needs to be ratified by the General Body

## Structure
Legally all Societies registered in India need to have an executive committee of atleast seven members. Within this seven, there has to be a President, Secretary and Treasurer. 

- Executive Committe 


## Office Bearers
- President
- Secretary
- Treasurer

# GLUGs

# Primary Unit

# Meetings
## GB
## EC

# Bye-Laws

# Amendment
Constitution can be amended by 2/3 majority vote of the General Body in quorum
